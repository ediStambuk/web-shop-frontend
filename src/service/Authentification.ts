import Axios from "axios";

export function login(username: string, password: string) {
  return Axios.post("/api/login", {userName: username, password: password});
}

export function register(username: string, password: string) {
  return Axios.post("/api/register", {username: username, password: password, cartId: 1});
}
