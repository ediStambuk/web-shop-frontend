import React from 'react';
import {Router, Route} from 'react-router-dom';
import './App.css';
import {WebShop} from "./pages/WebShop";
import {Login} from "./pages/Login";
import {Registration} from "./pages/Registration";
import history from './history';

function App() {

  return (
    <Router history={history}>
      <Route exact={true} path="/login">
        <Login/>
      </Route>
      <Route exact={true} path="/registration">
        <Registration/>
      </Route>
      <Route path="/" >
        <WebShop/>
      </Route>
    </Router>
  );
}

export default App;
