import React, {ChangeEvent} from 'react';
import {register} from "../service/Authentification";
import history from "../history";
import {Alert, Button, Form} from "react-bootstrap";

export class Registration extends React.Component {
  state = {
    emptyData: false,
    wrongRepeatPasswords: false
  };

  private username = "";
  private password = "";
  private repeatPassword = "";

  onChangeUsername(event: ChangeEvent<HTMLInputElement>) {
    this.username = event.target.value;
  }

  onChangePassword(event: ChangeEvent<HTMLInputElement>) {
    this.password = event.target.value;
  }

  onChangePasswordSecond(event: ChangeEvent<HTMLInputElement>) {
    this.repeatPassword = event.target.value;
  }

  public submit() {
      if (this.username.length > 0 && this.password.length > 0
          && this.repeatPassword.length > 0) {
        if (this.password === this.repeatPassword) {
          register(this.username, this.password)
              .then((response) => {
                if (response.status === 201) {
                  history.push('/login');
                }
              });
        } else {
          this.setState({wrongRepeatPasswords: true, emptyData: false});
        }
      } else {
        this.setState({emptyData: true, wrongRepeatPasswords: false});
      }
    }

    render() {
      return(
          <Form>
            {this.state.emptyData && <Alert key="emptyData" variant="warning">
                Username or password is empty
            </Alert>}
            {this.state.wrongRepeatPasswords && <Alert key="emptyData" variant="warning">
                Repeated password is not equal to original password
            </Alert>}

            <Form.Group controlId="formBasicUsername">
              <Form.Label>Username</Form.Label>
              <Form.Control type="text" placeholder="Enter username"
                            onChange={this.onChangeUsername.bind(this)}/>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password"
                            onChange={this.onChangePassword.bind(this)}/>
            </Form.Group>

            <Form.Group controlId="formBasicPasswordSecond">
              <Form.Label>Repeat password</Form.Label>
              <Form.Control type="password" placeholder="Repeat password"
                            onChange={this.onChangePasswordSecond.bind(this)}/>
            </Form.Group>

            <Button variant="primary" type="button"
                    onClick={this.submit.bind(this)}>
              Submit
            </Button>
          </Form>
      );
    }
}
