import React, {ChangeEvent} from 'react';
import {login} from "../service/Authentification";
import history from '../history';
import {Alert, Button, Form} from "react-bootstrap";
import {Link} from "react-router-dom";

export class Login extends React.Component {

  state = {
    emptyData: false,
    notFoundCredentials: false
  };

  private username = "";
  private password = "";

  onChangeUsername(event: ChangeEvent<HTMLInputElement>) {
    this.username = event.target.value;
  }

  onChangePassword(event: ChangeEvent<HTMLInputElement>) {
    this.password = event.target.value;
  }

  public submit() {
    if (this.username.length > 0 && this.password.length > 0) {
      login(this.username, this.password)
          .then((response) => {
            if (response.status === 200) {
              sessionStorage.setItem("jwt", response.data);
              history.push('/');
            }
          }).catch(error => {
              this.setState({notFoundCredentials: true, emptyData: false});
          });
    } else {
      this.setState({emptyData: true, notFoundCredentials: false});
    }
  }

  render() {
    return(
      <Form>
        {this.state.emptyData && <Alert key="emptyData" variant="warning">
          Username or password is empty
        </Alert>}
        {this.state.notFoundCredentials && <Alert key="emptyData" variant="warning">
            There is no such user or it is wrong password
        </Alert>}

        <Form.Group controlId="formBasicUsername">
          <Form.Label>Username</Form.Label>
          <Form.Control type="text" placeholder="Enter username"
          onChange={this.onChangeUsername.bind(this)}/>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password"
          onChange={this.onChangePassword.bind(this)}/>
        </Form.Group>

        <Button variant="primary" type="button"
        onClick={this.submit.bind(this)}>
          Submit
        </Button>

        <Link to="/registration" color="blue">
          You don't have an account? register
        </Link>
      </Form>
    );
  }
}
