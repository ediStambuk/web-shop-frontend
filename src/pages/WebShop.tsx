import React from 'react';
import {Nav, Navbar} from "react-bootstrap";
import {Switch, Route} from 'react-router-dom';
import {CartContent} from "../components/CartContent";
import {Products} from "../components/Products";

export class WebShop extends React.Component<any, any> {
  render() {
    return(
        <>
          <Navbar bg="primary" variant="dark">
            <Navbar.Brand href="/">Navbar</Navbar.Brand>
            <Nav className="mr-auto">
            </Nav>
            <Nav>
              <Nav.Link href="/login">Log in</Nav.Link>
              <Nav.Link href="/cart">Cart</Nav.Link>
            </Nav>
          </Navbar>

          <Switch>
            <Route exact={true} to="/cart">
              <CartContent/>
            </Route>
            <Route exact={true} to="/">
              <Products/>
            </Route>
          </Switch>
        </>
    );
  }
}
